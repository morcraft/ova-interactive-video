const _ = require('lodash')
const videoActionsEvaluator = require('./videoActionsEvaluator.js')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected.')
    }

    if(!_.isObject(args.player)){
        throw new Error('A player is expected.')
    }

    if(!_.isObject(args.videoSteps)){
        throw new Error('Some video steps are expected.')
    }

    if(_.isUndefined(args.videoActionsEvaluator)){
        throw new Error('A video evaluator is expected.')
    }

    args.player.setVolume(100)
    args.player.stopVideo()
    args.player.on('stateChange', function (event) {
        var state = args.player.getPlayerState();
        state.then(function (value) {
            args.videoActionsEvaluator.evaluate({
                state: state,
                value: value,
                event: event,
                videoSteps: args.videoSteps
            })
        })
    });
}