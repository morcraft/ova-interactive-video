const swal = require('sweetalert2');
const _ = require('lodash');
const swalOptions = require('swalOptions.js');
const YouTubePlayer = require('youtube-player');

module.exports = function (args) {
    if (!_.isObject(args)) {
        throw new Error('Some arguments are expected.');
    }

    if (!_.isString(args.elementId)) {
        throw new Error('An element ID is expected in the DOM to append YouTube iframe.');
    }

    if (!_.isString(args.videoId)) {
        throw new Error('A YouTube video ID is expected.');
    }

    var modal = null;

    if(args.defaultBehavior !== false){
        if (!_.isFunction(args.template)) {
            throw new Error('A template is expected to render modal.');
        }
        
        modal = swal(_.merge(swalOptions, {
            html: args.template(),
            customClass: 'ova-themed video interactive'
        }))

    }

    var element = document.getElementById(args.elementId);
    return {
        player: new YouTubePlayer(element, {
            videoId: args.videoId,
            playerVars: _.merge({
                rel: 0,
                fs: 0
            }, args.playerVars)
        }),
        modal: modal
    }
}