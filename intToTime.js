module.exports = function(time) {
    //https://stackoverflow.com/a/11486026
    var hrs = Math.floor(time / 3600);
    var mins = Math.floor((time % 3600) / 60);
    var secs = time % 60;
    var ret = '';
    if (hrs > 0) {
      ret += '' + hrs + '' + (mins < 10 ? '' : '');
    }
  
    if(mins > 0){
      ret += '' + mins + '' + (secs < 10 ? '' : '');
    }
    ret += '' + secs;
    return ret;
  }