const _ = require('lodash');
const addInteractiveVideo = require('./addInteractiveVideo.js')
const videoActionsEvaluator = require('./videoActionsEvaluator.js')
const setDefaultPlayerBehavior = require('./setDefaultPlayerBehavior.js')
const timeToInt = require('./timeToInt.js')
const intToTime = require('./intToTime.js')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected.')
    }

    var self = this;
    var interactiveVideo = addInteractiveVideo(args);
    self.videoActionsEvaluator = new videoActionsEvaluator();
    self.timeToInt = timeToInt;
    self.intToTime = intToTime;
    self.player = interactiveVideo.player;
    self.modal = interactiveVideo.modal;
    
    setDefaultPlayerBehavior({
      player: interactiveVideo.player,
      videoActionsEvaluator: self.videoActionsEvaluator,
      videoSteps: args.videoSteps
    })
}