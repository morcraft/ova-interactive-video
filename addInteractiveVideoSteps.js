const _ = require('lodash')
const timeToInt = require('./timeToInt.js')

module.exports = function (args) {
    if (!_.isObject(args)) {
        throw new Error('Some arguments are expected.')
    }

    if(!_.isObject(args.event)){
        throw new Error('A video event is expected.')
    }
    
    if(!_.isObject(args.videoSteps)){
        throw new Error('Some video steps are expected.')
    }

    var self = this;
    var currentTime = args.event.target.getCurrentTime()
    var _default = true;
    _.forEach(args.videoSteps, function (v, k) {
        if (_.isArray(v.range)) {
            if (_.inRange(currentTime, timeToInt(v.range[0]), timeToInt(v.range[1]))) {
                _default = false;
                if (!v.active) {
                    if(!_.isFunction(v.do)){
                        throw new Error('A function is expected in "do" property')
                    }

                    v.do(args.event)
                    if (v.multipleTimes !== true) {
                        v.active = true;
                    }
                }
            }
        }
    })

    if (_default) {
        if (_.isFunction(_.get(args, 'videoSteps.default.do'))) {
            args.videoSteps.default.do(event)
        }
    }
}